package com.dbnis.dev.webviewgpstracker;

import android.app.NotificationManager;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    private String TAG = "FirebaseMessagingService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());


        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            String title = remoteMessage.getNotification().getTitle();
            String message = remoteMessage.getNotification().getBody();
            String click_action = remoteMessage.getNotification().getClickAction();
            //notifyUser(title,message,click_action);


        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        //TODO: get the incoming payload

        try{
            JSONObject data = new JSONObject(remoteMessage.getData());
            String type = data.getString("type");
            String url = data.getString("url");
            Log.d(TAG,"click_action"+type);

            notifyUser(
                    remoteMessage.getNotification().getTitle(),
                    remoteMessage.getNotification().getBody(),
                    type,url);
            Log.d(TAG,"Recieved extra info"+type);
        }catch (JSONException e){
            e.printStackTrace();
        }
//        notifyUser(remoteMessage.getFrom(),remoteMessage.getNotification().getBody(),);
    }

    //TODO: NOTIFY USER
    public void notifyUser(String from, String body,String type,String payload){
        CustomNotificationManager customNotificationManager = new CustomNotificationManager(getApplicationContext());
        customNotificationManager.showNotification(from,body,type,payload);
    }
}
