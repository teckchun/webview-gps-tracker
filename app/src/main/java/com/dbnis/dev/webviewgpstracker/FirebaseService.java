package com.dbnis.dev.webviewgpstracker;

import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class FirebaseService extends FirebaseInstanceIdService {

    String TAG = FirebaseService.class.getSimpleName();
    public static final String TOKEN_BROADCAST = "fcmtokenbroadcast";
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        //TODO: to generate firebase Token
        // this method is only called when token is change or newly generated
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
       // sendRegistrationToServer(refreshedToken);

        //TODO: send the Broadcast to main activity when token is genrated.
        getApplicationContext().sendBroadcast(new Intent(TOKEN_BROADCAST));

        saveToken(refreshedToken);



    }
    // [END refresh_token]

    //TODO: call the save token method from SharePrefManager
    private void saveToken(String token){
        SharePrefManager.getInstance(getApplicationContext()).saveToken(token);
    }


    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
    }
}
