package com.dbnis.dev.webviewgpstracker;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class CustomNotificationManager {

    private Context mContext;
    public static final int NOTIFICATION_ID = 123;

    public CustomNotificationManager(Context context){
        this.mContext = context;
    }

    public void showNotification(String from, String body, String type,String payload){
        Intent intent;
//        if(type.equals("CUSTOMACTIVITY")){
//            intent = new Intent(mContext,CustomActivity.class);
//            intent.putExtra("title",from);
//            intent.putExtra("body",body);
//            intent.putExtra("url",payload);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        }else

        if(type.equals("1")){
            intent = new Intent(mContext,MainActivity.class);
            intent.putExtra("url",payload);
            Log.e("CustomNotification","type=1");

            intent.addFlags((Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }else{
            intent = new Intent(mContext,MainActivity.class);
            Log.e("CustomNotification","type=0");
            intent.addFlags((Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        PendingIntent pendingIntent = PendingIntent.getActivity(
                mContext,
                NOTIFICATION_ID,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext);
        Notification mNotification = builder.setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setContentTitle(from)
                .setContentText(body)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(),R.mipmap.ic_launcher))
                .build();

        mNotification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(NOTIFICATION_ID,mNotification);




        /*PendingIntent pendingIntent = PendingIntent.getActivity(
                mContext,
                NOTIFICATION_ID,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext);
        Notification mNotification = builder.setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setContentTitle(from)
                .setContentText(notification)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(),R.mipmap.ic_launcher))
                .build();
        mNotification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(NOTIFICATION_ID,mNotification);
        */



    }



}
