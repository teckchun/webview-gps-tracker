package com.dbnis.dev.webviewgpstracker;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePrefManager {

    private static final String SHARE_PREF_NAME = "fcmsharedpref";
    private static final String KEY_ACCESS_TOKEN = "token";


    private static Context mContext;
    private static  SharePrefManager mInstance;

    private SharePrefManager(Context context){
        mContext = context;
    }

    public static synchronized SharePrefManager getInstance(Context contex){

        if(mInstance==null)
            mInstance = new SharePrefManager(contex);

        return mInstance;
    }



    //TODO: Save Token
    public boolean saveToken(String token){
        SharedPreferences sharedPreferences =  mContext.getSharedPreferences(SHARE_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_ACCESS_TOKEN,token);
        editor.apply();

        return true;
    }

    //TODO: Get Token
    public String getToken(){
        SharedPreferences sharedPreferences =  mContext.getSharedPreferences(SHARE_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_ACCESS_TOKEN,null);

    }


}
