package com.dbnis.dev.webviewgpstracker;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.widget.TextView;
import android.widget.Toast;

import com.mattprecious.swirl.SwirlView;

/**
 * Created by whit3hawks on 11/16/16.
 */
@RequiresApi(api = Build.VERSION_CODES.M)
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

    private Context context;

    // Constructor
    public FingerprintHandler(Context mContext) {
        context = mContext;

    }

    @SuppressLint("MissingPermission")
    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
        CancellationSignal cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        //this.update("Fingerprint Authentication error\n" + errString);
      //  this.update("ERROR");


    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        //this.update("Fingerprint Authentication help\n" + helpString);

    }

    @Override
    public void onAuthenticationFailed() {

        //this.update("Fingerprint Authentication failed.");
        Toast.makeText(context,"Finger Print Un-Matched",Toast.LENGTH_LONG).show();
        this.update("ERROR");



    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        ((Activity) context).finish();
        Toast.makeText(context,"Finger Print Matched",Toast.LENGTH_LONG).show();
        this.update("OFF");

        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    private void update(String e){
        SwirlView swirlView = (SwirlView) ((Activity)context).findViewById(R.id.swirlView);
        swirlView.setState(SwirlView.State.valueOf(e));

    }



}
