package com.dbnis.dev.webviewgpstracker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class CustomActivity extends AppCompatActivity {
    private TextView txtPayload,txtTitle,txtBody;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom);

        txtTitle = (TextView)findViewById(R.id.txtTitle);
        txtBody = (TextView)findViewById(R.id.txtBody);
        txtPayload = (TextView)findViewById(R.id.txtPayload);


        Intent getI=getIntent();
        String title =getI.getStringExtra("title");
        String body =getI.getStringExtra("body");
        String payload =getI.getStringExtra("payload");

        txtTitle.setText("Title: "+title);
        txtBody.setText("Body: "+body);
        txtPayload.setText("Payload Received: "+payload);


    }
}
